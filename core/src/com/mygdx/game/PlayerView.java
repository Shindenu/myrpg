package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import java.util.ArrayList;
import java.util.List;

/**
 * Отображение игрока
 */
public class PlayerView extends UnitView {

    PlayerView(String[] textures){
            super(textures[0], textures[1], textures[2], textures[3], textures[4], textures[5]);
    }
    private Texture iventoryIcon=new Texture("inventory.png");
    private Texture inventoryBackground=new Texture("inventory_background.png");

    BitmapFont font = new BitmapFont();
    public void draw (SpriteBatch batch, Position pos, int currentHp, int hp, int level, int xp, int agility, int power, int defense) {
        font.setColor(Color.YELLOW);
        //иконка
        super.draw(batch,pos);
        //уровень
        font.draw(batch, "Level: "+String.valueOf(level) , 15, 675);
        //опыт
       font.draw(batch, "XP:"+String.valueOf(xp)+"/100" , 15, 650);
        //хп
        font.draw(batch, "HP:"+String.valueOf(currentHp)+"/"+String.valueOf(hp) , 15, 625);
        //уклонение
        font.draw(batch, "Agility:"+String.valueOf(agility) , 15, 600);
        //сила
        font.draw(batch, "Power:"+String.valueOf(power) , 15, 575);
        //защита
        font.draw(batch, "Defense:"+String.valueOf(defense) , 15, 550);
        batch.draw(iventoryIcon, 15.0f, 450.0f);
        //хп
        font.setColor(Color.WHITE);
        font.draw(batch, "HP:"+String.valueOf(currentHp)+"/"+String.valueOf(hp) , pos.getX(), pos.getY()-10);
    }

    public void drawInventory(SpriteBatch batch, List<ItemController> items){
        batch.draw(inventoryBackground,350, 20);
        int y = 500;
        font.setColor(Color.YELLOW);
        for (ItemController item : items) {
            batch.draw(item.getItemView().getSprite(), 500.f, (float) y-20);
            font.draw(batch, item.getItem().getName() + " power:" + String.valueOf(item.getItem().getPower()) + " hp:" + String.valueOf(item.getItem().getHp()) + " agility:" + String.valueOf(item.getItem().getAgility()), 550, y);
            y -= 40;
        }
    }
    public boolean isAttacking(){
        if (super.currentPose==Pose.ATTACK)
            return true;
        return false;
    }

}
