package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Предмет
 */
public class ItemController {

    private Field field;
    private Item item;
    private View itemView;

    ItemController(){
        item=new Item("Power potion",5,5,0);
        itemView=new View("Item.png");
    }


    /** Надеть предмет на игрока
     * @param unit - игрок
     */
    public void putOnAPlayer (UnitController unit){
        int newhp=unit.getModel().getCurrentHp()+this.getItem().getHp();
        unit.getModel().setCurrentHp(newhp);
        unit.getModel().setPower(unit.getModel().getPower()+this.getItem().getPower());
        unit.getModel().setAgility(unit.getModel().getAgility()+this.getItem().getAgility());
        field.removeItem(this);
        item.setPosition(new Position(-1,-1));
    }

    /** Снять предмет
     * @param unit - игрок
     */
    public void removeFrom (UnitController unit){
        unit.getModel().setHp(unit.getModel().getHp()-this.getItem().getHp());
        unit.getModel().setPower(unit.getModel().getPower()-this.getItem().getPower());
        unit.getModel().setAgility(unit.getModel().getAgility()-this.getItem().getAgility());
    }

    public Item getItem() {
        return this.item;
    }

    public View getItemView() {
        return itemView;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void setItemView(View itemView) {
        this.itemView = itemView;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }
    public void draw(SpriteBatch batch) {
        itemView.draw(batch, item.getPosition());
    }
}
