package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Игровое поле
 */
public class Field {
    private final int width=1250;
    private final int height=700;
    private Texture texture = new Texture("map.jpg");
    List<UnitController> units = new ArrayList<UnitController>();
    List<ItemController> items = new ArrayList<ItemController>();
    List<Teleport> teleports = new ArrayList<Teleport>();

    public Field() {
    }

    /**
     * Определить есть ли на данной позиции персонаж
     * @param curPos - позиция для проверки
     * @return - персонаж на данной позиции
     */
    public UnitController checkUnit (Position curPos){
        UnitController tmp;
        Iterator<UnitController> iter=units.iterator();
        while (iter.hasNext()) {
            tmp=iter.next();
            if (Math.abs(curPos.getX() - tmp.getModel().getPosition().getX()-tmp.getModel().getSize().getX()) < 100
                    && Math.abs(curPos.getY()- tmp.getModel().getPosition().getY() -tmp.getModel().getSize().getY()) < 100)
                return tmp;
        }
        tmp=null;
        return tmp;
    }

    /**
     * Определить есть ли на данной позиции предмет
     * @param curPos - позиция для проверки
     * @return - предмет на данной позиции
     */
    public ItemController checkItem (Position curPos){
        ItemController tmp;
        Iterator<ItemController> iter=items.iterator();
        while (iter.hasNext()) {
            tmp=iter.next();
            if (Math.abs(curPos.getX() - tmp.getItem().getPosition().getX()-tmp.getItem().getSize().getX()) < 100
                    && Math.abs(curPos.getY()- tmp.getItem().getPosition().getY()-tmp.getItem().getSize().getY()) < 100)
                return tmp;
        }
        tmp=null;
        return tmp;
    }
    /**
     * Определить есть ли на данной позиции предмет
     * @param curPos - позиция для проверки
     * @return - предмет на данной позиции
     */
    public Teleport checkTeleports (Position curPos){
        Teleport tmp;
        Iterator<Teleport> iter=teleports.iterator();
        while (iter.hasNext()) {
            tmp=iter.next();
            if ((Math.abs(curPos.getX() - tmp.getFrom().getX()-tmp.getSize().getX()) < 100
                    && Math.abs(curPos.getY()- tmp.getFrom().getY()-tmp.getSize().getY()) < 100) ||
                    (Math.abs(curPos.getX() - tmp.getTo().getX()-tmp.getSize().getX()) < 100
                            && Math.abs(curPos.getY()- tmp.getTo().getY()-tmp.getSize().getY()) < 100))
                return tmp;
        }
        tmp=null;
        return tmp;
    }

    /**
     * Удалить предмет с поля
     * @param item - предмет
     */
    public void removeItem (ItemController item){
        Iterator<ItemController> it = getItems().iterator();
        while (it.hasNext())
        {
            if(it.next()==item)
                it.remove();
        }
    }

    /**
     * Удалить персонажа с поля
     * @param - персонаж
     */
    public void removeUnit (UnitController u){
        Iterator<UnitController> it = getUnits().iterator();
        while (it.hasNext())
        {
            if(it.next()==u)
                it.remove();
        }
    }

    public void addUnit(UnitController u){
        units.add(u);
    }

    public Texture getTexture(){
        return texture;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public List<UnitController> getUnits() {
        return units;
    }

    public List<ItemController> getItems() {
        return items;
    }

    public void addItem (ItemController item){
        items.add(item);
    }

    public void addTeleport (Teleport tp) { teleports.add(tp);}

    public List<Teleport> getTeleports() {
        return teleports;
    }
}
