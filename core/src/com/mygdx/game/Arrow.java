package com.mygdx.game;

/**
 * Стрела
 */
public class Arrow {
    //Позиция
    private Position pos;
    //Угол поворота
    private float angle;

    Arrow(Position position, float rotate){
        pos=position;
        angle=rotate;
    }

    public Position getPosition(){
        return pos;
    }

    public float getAngle() {
        return angle;
    }

    public void setCurPos(Position curPos) {
        this.pos = curPos;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }
}
