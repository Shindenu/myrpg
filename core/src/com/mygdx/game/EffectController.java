package com.mygdx.game;


/**
 * Временный эффект
 */
public class EffectController extends ItemController {

    //Время действия эффекта
    private int time;
    EffectController ec =this;
    EffectController(){
        super.setItem(new Item("Buff", 0, 20,12));
        super.getItem().setPosition(new Position(-1,-1));
        this.time=10;
    }

    @Override
    public void putOnAPlayer (UnitController unit){
        int newhp=unit.getModel().getCurrentHp()+this.getItem().getHp();
        unit.getModel().setCurrentHp(newhp);
        unit.getModel().setPower(unit.getModel().getPower()+this.getItem().getPower());
        unit.getModel().setAgility(unit.getModel().getAgility()+this.getItem().getAgility());
        Timer tickThread = new Timer(unit);
        tickThread.start();
    }

    public class Timer extends Thread {
        UnitController unit;
        Timer (UnitController u){
            unit=u;
        }
        public void run() {
            try {
                Thread.sleep(ec.getTime()*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ec.removeFrom(unit);
            return;
        }
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

}
