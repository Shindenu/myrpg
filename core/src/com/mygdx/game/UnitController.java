package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Random;

/**
 * Игровой персонаж
 */
abstract public class UnitController {
    private UnitView view;
    private Unit model;
    private Field field;

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    /**
     * Двигаться в определенном направлении
     * @param d - направление движения
     */
    public void move(Direction d) {
        Position newPos = model.getPosition();
        switch (d) {
            case NORTH:
                    newPos.setY(newPos.getY() + 1);
                break;
            case SOUTH:
                    newPos.setY(newPos.getY() - 1);
                break;
            case WEST:
                    newPos.setX(newPos.getX() - 1);
                break;
            case EAST:
                    newPos.setX(newPos.getX() + 1);
                break;
        }
    }

    /**
     * Атаковать
     * @param enemy - противник
     */
    public void attack(UnitController enemy) {
        int damage=enemy.model.getCurrentHp()-this.model.getPower()+enemy.model.getDefense();
        getView().setAttack();
        Random rnd=new Random();
        //Если персонаж не промахнулся
        if(rnd.nextInt(100)>enemy.getModel().getAgility())
            //Уменьшить жизнь противнику
            enemy.model.setCurrentHp(damage);
        //Среагировать на атаку
        enemy.react(this);
    }

    /**
     * Среагировать
     * @param aggressor - противник
     */
    abstract public void react(UnitController aggressor);

    /**
     * Умереть
     */
    public void die() {
       getField().removeUnit(this);
    }

    public void draw(SpriteBatch batch) {
        view.draw(batch, model.getPosition());
    }

    public Unit getModel(){
        return model;
    }

    public UnitView getView() {
        return view;
    }

    public void setModel(Unit model) {
        this.model = model;
    }

    public void setView(UnitView view) {
        this.view = view;
    }

}
