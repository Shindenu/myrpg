package com.mygdx.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Игрок
 */
public class Player extends Unit {

    private int experience;
    private int level;
    private int currentPower;
    private int currentAgility;

    List<ItemController> inventory = new ArrayList<ItemController>();

    Player(){
        super.setPosition(new Position(625-50, 350-50));
        super.setSize(new Size(100,100));
        super.setRange(100);
        super.setPower(20);
        super.setHp(200);
        super.setCurrentHp(200);
        super.setAgility(80);
        super.setDefense(10);
        setLevel(1);
    }

    public List<ItemController> getInventory() {
        return inventory;
    }

    public int getCurrentAgility() {
        return currentAgility;
    }

    public int getCurrentPower() {
        return currentPower;
    }

    public int getExperience() {
        return experience;
    }

    public int getLevel() {
        return level;
    }

    public void setExperience(int exp){
        experience=exp;
        if (experience>100)
            setLevel(2);
    }

    public void setCurrentAgility(int currentAgility) {
        this.currentAgility = currentAgility;
    }

    public void setCurrentPower(int currentPower) {
        this.currentPower = currentPower;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setInventory(List<ItemController> inventory) {
        this.inventory = inventory;
    }

}
