package com.mygdx.game;

/**
 * Монстр
 */
public class Monster extends Unit {
    private int price;

    Monster(){
        super.setPosition(Position.getRandomPos());
        super.setSize(new Size(100, 100));
        super.setHp(100);
        super.setPower(15);
        super.setRange(50);
        super.setCurrentHp(100);
        super.setAgility(50);
        super.setDefense(0);
        setPrice(20);
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
