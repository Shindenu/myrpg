package com.mygdx.game;

/**
 * Отображение монстра
 */
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MonsterView extends UnitView {

    MonsterView(String texture){
        super(texture);
        FRAME_COLS = new int[]{6,6,6,6,6,6};
        frameDuration = 0.3f;
        constructFrames();
    }

    public void draw (SpriteBatch batch, Position pos, int currentHp, int hp) {
        super.draw(batch,pos);
        font.setColor(Color.WHITE);
        //хп
        font.draw(batch, "HP:"+String.valueOf(currentHp)+"/"+String.valueOf(hp) , pos.getX()-10,pos.getY()-10);
    }

}
