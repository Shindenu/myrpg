package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.*;

/**
 * Отображение юнита
 */
public class UnitView {

    enum Pose {LEFT, RIGHT, UP, DOWN, STAND, ATTACK};
    Pose currentPose = Pose.STAND;
    List<String> animationsPaths = new LinkedList<String>();
    float frameDuration = 0.15f;
    protected int FRAME_COLS[] = {8,8,8,8,16,16};
    protected int FRAME_ROWS = 1;
    protected Map<String,Animation> animations = new HashMap<String,Animation>();
    protected TextureRegion currentFrame;
    protected float stateTime;
    protected BitmapFont font = new BitmapFont();

    protected UnitView(String up, String down, String left, String right, String standing, String attacking) {
        font.setColor(255.0f, 255.0f, 255.0f, 255.0f);
        animationsPaths.add(up);
        animationsPaths.add(down);
        animationsPaths.add(left);
        animationsPaths.add(right);
        animationsPaths.add(standing);
        animationsPaths.add(attacking);
        constructFrames();
    }

    protected UnitView(String defaultPose) {
        font.setColor(255.0f, 255.0f, 255.0f, 255.0f);
        for (int i=0; i<6; i++)
            animationsPaths.add(defaultPose);
        constructFrames();
    }

    public void draw(SpriteBatch batch, Position pos) {
        stateTime += Gdx.graphics.getDeltaTime();
        switch (currentPose){
            case LEFT:
                currentFrame = (TextureRegion) animations.get("left").getKeyFrame(stateTime, true);
                break;
            case RIGHT:
                currentFrame = (TextureRegion) animations.get("right").getKeyFrame(stateTime, true);
                break;
            case UP:
                currentFrame = (TextureRegion) animations.get("up").getKeyFrame(stateTime, true);
                break;
            case DOWN:
                currentFrame = (TextureRegion) animations.get("down").getKeyFrame(stateTime, true);
                break;
            case STAND:
                currentFrame = (TextureRegion) animations.get("standing").getKeyFrame(stateTime, true);
                break;
            case ATTACK:
                currentFrame = (TextureRegion) animations.get("attack").getKeyFrame(stateTime, true);
                break;
        }
        batch.draw(currentFrame, pos.getX(), pos.getY());
    }

    public void setLeftMove() {
        if (currentPose != Pose.LEFT && currentPose != Pose.ATTACK) {
            currentPose = Pose.LEFT;
        }
    }

    public void setRightMove() {
        if (currentPose != Pose.RIGHT && currentPose != Pose.ATTACK) {
            currentPose = Pose.RIGHT;
        }
    }

    public void setUpMove() {
        if (currentPose != Pose.UP && currentPose != Pose.ATTACK) {
            currentPose = Pose.UP;
        }
    }

    public void setDownMove() {
        if (currentPose != Pose.DOWN && currentPose != Pose.ATTACK) {
            currentPose = Pose.DOWN;
        }
    }

    public void setStand() {
        currentPose = Pose.STAND;
    }

    public void setAttack() {
        if (currentPose != Pose.ATTACK) {
            currentPose=Pose.ATTACK;
            AttackTimer th = new AttackTimer();
            th.start();
        }
    }

    public class AttackTimer extends Thread {
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            setStand();
            return;
        }
    }

    protected void constructFrames() {
        Random rand = new Random();
        String[] animationsNames = {"up","down","left","right", "standing", "attack"};
        for (int i = 0; i<6; i++) {
            Texture tmpSheet = new Texture(Gdx.files.internal(animationsPaths.get(i)));
            TextureRegion[][] tmpRegion = TextureRegion.split(tmpSheet, tmpSheet.getWidth() / FRAME_COLS[i], tmpSheet.getHeight() / FRAME_ROWS); // #10
            TextureRegion[] tmpFrames = new TextureRegion[FRAME_COLS[i] * FRAME_ROWS];
            int index = 0;
            for (int j = 0; j < FRAME_ROWS; j++) {
                for (int k = 0; k < FRAME_COLS[i]; k++) {
                    tmpFrames[index++] = tmpRegion[j][k];
                }
            }
            if (i==5) {
                animations.put(animationsNames[i], new Animation(0.0625f, tmpFrames));
            }
            else
                animations.put(animationsNames[i], new Animation(frameDuration, tmpFrames));
        }
        stateTime = 0 + rand.nextFloat() * (1f - 0);
    }
}
