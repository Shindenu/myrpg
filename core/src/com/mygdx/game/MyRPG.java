package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.Input.Keys;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyRPG extends ApplicationAdapter {
    SpriteBatch batch;
    Texture img;
    Field map;
    Texture arrowPic;
    Texture startMenu;
    Texture restartMenu;
    PlayerController player;
    List<MonsterController> monsters=new ArrayList<MonsterController>();
    List<ItemController> items = new ArrayList<ItemController>();
    List<Teleport> teleports = new ArrayList<Teleport>();
    boolean gameStarted=false;
    boolean playerDied=false;

    private void createMonsters() {
        for (int i = 0; i < 5; i++) {
            MonsterController mob = new MonsterController(MobType.AXE);
            monsters.add(mob);
            mob.setField(map);
            map.addUnit(mob);
        }
        for (int i = 0; i < 3; i++) {
            MonsterController mob = new MonsterController(MobType.SCYTHE);
            monsters.add(mob);
            mob.setField(map);
            map.addUnit(mob);
        }
    }

    @Override
    public void create() {
        map = new Field();
        arrowPic=new Texture("arrow.png");
        startMenu = new Texture("startPage.png");
        restartMenu = new Texture("gameOver.png");
        Gdx.input.setInputProcessor(new InputAdapter(){
            public boolean touchDown(int x,int y,int pointer,int button){
                if (gameStarted &&!playerDied )
                    player.tryToInteract(x,y);// код при нажатии
                else
                    interact (x,y);
                return true; // возвращает true, сообщая, что событие было обработано
            }});
        createMonsters();
        for (int i=0; i<3; i++)
        {
            items.add(new ItemController());
            items.get(i).setField(map);
            map.addItem(items.get(i));
        }
        Teleport tp= new Teleport();
        tp.setField(map);
        teleports.add(tp);
        map.addTeleport(tp);
        batch = new SpriteBatch();

    }

    @Override
    public void render() {
        if (!playerDied && gameStarted) {
            batch.begin();
            if (Gdx.input.isKeyPressed(Keys.LEFT)) player.move(Direction.WEST);
            else if (Gdx.input.isKeyPressed(Keys.RIGHT)) player.move(Direction.EAST);
            else if (Gdx.input.isKeyPressed(Keys.UP)) player.move(Direction.NORTH);
            else if (Gdx.input.isKeyPressed(Keys.DOWN)) player.move(Direction.SOUTH);
            else player.stay();
            batch.draw(map.getTexture(), 0, 0);
            Gdx.gl.glClearColor(0, 0, 0, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            for (ItemController item : map.getItems())
                item.draw(batch);
            for (Teleport tp :map.getTeleports())
                tp.draw(batch);
            for (UnitController unit : map.getUnits()) {
                unit.draw(batch);
            }
            if (player.getType() == PlayerType.ARCHER) {
                for (Arrow arrow : new ArrayList<Arrow>(player.getArrows()))
                    batch.draw(arrowPic, arrow.getPosition().getX(), arrow.getPosition().getY(), 6,
                            50, 12, 100, 1, 1,
                            arrow.getAngle(), 0, 0, 12, 100, false, false);
            }

            batch.end();
        }
        else if (!gameStarted){
            batch.begin();
            batch.draw(startMenu,0,0);
            batch.end();
        }
        else if (playerDied){
            batch.begin();
            batch.draw(restartMenu,0,0);
            batch.end();
        }
    }

    public void interact(int x, int y) {
        if (!gameStarted && x > 480 && y > 180 && x < 780 && y < 280) {
            gameStarted=true;
            player = new PlayerController(PlayerType.ARCHER);
            map.addUnit(player);
            player.setField(map);
        }
        else if (!gameStarted && x > 480 && y > 400 && x < 780 && y < 500){
            gameStarted=true;
            player = new PlayerController(PlayerType.KNIGHT);
            map.addUnit(player);
            player.setField(map);
        }
        if (playerDied && x > 480 && y > 270 && x < 780 && y < 400) {
            gameStarted=true;
            playerDied=false;
            map.units.clear();
            map.items.clear();
            createMonsters();
            for (int i=0; i<3; i++)
            {
                items.add(new ItemController());
                items.get(i).setField(map);
                map.addItem(items.get(i));
            }
            map.addUnit(player);
            player.setField(map);
            batch = new SpriteBatch();
        }
    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
