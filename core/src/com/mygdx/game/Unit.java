package com.mygdx.game;

/**
 * Игровой персонаж
 */
abstract public class Unit {
    private int hp;
    private int currentHp;
    private int power;
    private int agility;    //1-100%
    private Position position;
    private Size size;
    private int range;
    private int defense;

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }


    public int getAgility() {
        return agility;
    }

    public int getCurrentHp() {
        return currentHp;
    }

    public int getHp() {
        return hp;
    }

    public int getPower() {
        return power;
    }

    public int getRange() {
        return range;
    }

    public Size getSize() {
        return size;
    }

    public Position getPosition() {
        return position;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public void setCurrentHp(int currentHp) {
        this.currentHp = currentHp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}
