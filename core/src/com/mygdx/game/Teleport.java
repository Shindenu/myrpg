package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/**
 * Телепорт
*/
public class Teleport {

    private Field field;
    private Position from;
    private Position to;
    private View teleportView;
    private Size size;

    Teleport(){
        from=Position.getRandomPos();
        to=Position.getRandomPos();
        teleportView=new View("gate.png");
        size=new Size(100,100);
    }

    public Position getFrom() {
        return from;
    }

    public Position getTo() {
        return to;
    }

    public void movePlayer(Position clicked, UnitController unit){
        if (clicked.isInRange(from, 100))
            unit.getModel().setPosition(new Position(to.getX(),to.getY()));
        if (clicked.isInRange(to,100))
            unit.getModel().setPosition(new Position(from.getX(),from.getY()));
    }

    public Size getSize() {
        return size;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public void draw(SpriteBatch batch) {
        teleportView.draw(batch, this.getFrom());
        teleportView.draw(batch, this.getTo());
    }
}
