package com.mygdx.game;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Отображение объекта
 */
public class View {
    Texture sprite;

    View(){}
    View(String path){
        setSprite(new Texture(path));
    }

    /**
     * Отрисовать объект
     * @param batch - спрайт
     * @param pos - позиция
     */
    public void draw(SpriteBatch batch, Position pos){
        batch.draw(sprite, pos.getX(), pos.getY());
    }

    public Texture getSprite() {
        return sprite;
    }

    public void setSprite(Texture sprite) {
        this.sprite = sprite;
    }
}
