package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


/**
 * Игрок
 */
public class PlayerController extends UnitController{

    boolean inventoryOpened=false;
    private PlayerType type;
    private List<Arrow> arrows=new ArrayList<Arrow>();

    PlayerController (PlayerType ptype){
        super.setModel(new Player());
        type=ptype;
        String[] textures=new String[6];
        //Если класс рыцарь
        if (ptype==PlayerType.KNIGHT) {
            textures[0] = "up.png";
            textures[1] = "down.png";
            textures[2] = "left.png";
            textures[3] = "right.png";
            textures[4] = "stay.png";
            textures[5] = "attackPlayer.png";
        }
        //Если класс лучник
        if (ptype==PlayerType.ARCHER) {
            textures[0] = "archer_up.png";
            textures[1] = "archer_down.png";
            textures[2] = "archer_left.png";
            textures[3] = "archer_right.png";
            textures[4] = "archer_stay.png";
            textures[5] = "archer_attack.png";
            getModel().setRange(550);
        }
        super.setView(new PlayerView(textures));

    }

    public PlayerType getType() {
        return type;
    }

    /**
     * Попытка взаимодействовать с местом нажатия
     * @param X - координата
     * @param Y - координата
     */
    public void tryToInteract(int X, int Y)
    {
        ItemController item;
        UnitController enemy;
        Teleport tp;
        //Определить позицию в которой был выполнен клик мыши
        Position clicked=new Position(X,700-Y);
        //Если нажата кнопка инвентарь
        if (clicked.getX() > 15 && clicked.getY() > 450 && clicked.getX()  < 85 && clicked.getY()  < 520) {
            if (inventoryOpened)
                inventoryOpened=false;
            else
                inventoryOpened=true;
        }
        //Если нажатие было выполнено в радиусе досягаемости
        else if (this.getModel().getPosition().isInRange(clicked,this.getModel().getRange()))
        {
            enemy=super.getField().checkUnit(clicked);
            //Если нажатие выполнено по врагу
            if (enemy instanceof MonsterController && enemy!=null)
            {
                //Атаковать врага
                attack(enemy);
            }
            item=super.getField().checkItem(clicked);
            //Если нажатие выполнено по предмету
            if(item instanceof ItemController && item!=null){
                //Надеть предмет
                addToInvenory(item);
                item.putOnAPlayer(this);
            }
            tp=super.getField().checkTeleports(clicked);
            if(tp!=null){
                tp.movePlayer(clicked,this);
            }
        }
    }

    /**
     * Реакция на атаку
     * @param aggressor - атакующий персонаж
     */
    public void react (UnitController aggressor){
        if(this.getModel().getCurrentHp()<1)
            this.die();
    }

    /**
     * Добавление предмета в инвентарь
     * @param newItem
     */
    public void addToInvenory(ItemController newItem){
        this.getModel().getInventory().add(newItem);
        newItem.putOnAPlayer(this);
    }

    /**
     * Удаление предмета из инвентаря
     * @param deletedItem - удаляемый предмет
     */
    public void deleteFromInventory (ItemController deletedItem){
        Iterator<ItemController> it=this.getModel().getInventory().iterator();
        while (it.hasNext()){
            if(it.next()==deletedItem) {
                deletedItem.removeFrom(this);
                it.remove();
            }
        }
    }
    public void stay() {
        if (!getView().isAttacking())
            getView().setStand();
    }
    /**
     * Двигаться в определенном направлении
     * @param d - направление движения
     */
    @Override
    public void move(Direction d) {
        Position newPos = this.getModel().getPosition();
        switch (d) {
            case NORTH:
                if(newPos.getY()<this.getField().getHeight()-this.getModel().getSize().getY()) {
                    this.getView().setUpMove();
                    newPos.setY(newPos.getY() + 3);
                }
                break;
            case SOUTH:
                if(newPos.getY()>0) {
                    this.getView().setDownMove();
                    newPos.setY(newPos.getY() - 3);
                }
                break;
            case WEST:
                if(newPos.getX()>0) {
                    this.getView().setLeftMove();
                    newPos.setX(newPos.getX() - 3);
                }
                break;
            case EAST:
                if(newPos.getX()<this.getField().getWidth()-this.getModel().getSize().getX()) {
                    this.getView().setRightMove();
                    newPos.setX(newPos.getX() + 3);
                }
                break;
        }
    }

    /**
     * Атаковать
     * @param enemy - противник
     */
    public void attack(UnitController enemy) {
        //Рассчитать урон наносимый противнику
        int damage=enemy.getModel().getCurrentHp()-this.getModel().getPower()+enemy.getModel().getDefense();
        getView().setAttack();
        if (this.type==PlayerType.ARCHER) {
            ArrowTimer th=new ArrowTimer(this.getModel().getPosition(),enemy.getModel().getPosition());
            th.start();
        }
        Random rnd=new Random();
        //Если персонаж не промахнулся
        if(rnd.nextInt(100)>enemy.getModel().getAgility())
            //Уменьшить жизнь противнику
            enemy.getModel().setCurrentHp(damage);
        //Среагировать на атаку
        enemy.react(this);
    }

    public List<Arrow> getArrows() {
        return arrows;
    }

    public class ArrowTimer extends Thread {
        Position startPos;
        Position endPos;

        public ArrowTimer(Position myPos, Position mobPos) {
            startPos=myPos;
            endPos=mobPos;
        }

        float step = 20;

        public void run() {
            //Находим расстояние между персонажем и противникиом
            float distance =(float) Math.sqrt(Math.pow(endPos.getX()-startPos.getX(),2)+Math.pow(endPos.getY()-startPos.getY(),2));
            float directionX = (endPos.getX()-startPos.getX()) / distance;
            float directionY = (endPos.getY()-startPos.getY()) / distance;
            Position a = new Position(startPos.getX(), endPos.getY());
            float x1 = endPos.getX()-startPos.getX();
            float x2 = a.getX() -startPos.getX();
            float y1 = endPos.getY()-startPos.getY();
            float y2 = a.getY()-startPos.getY();
            float d1 = (float) Math.sqrt (x1 * x1 + y1 * y1);
            float d2 = (float) Math.sqrt (x2 * x2 + y2 * y2);
            //Определяем угол поворота стрелы
            double acos = Math.acos((x1 * x2 + y1 * y2) / (d1 * d2));
            if ((x1>0 && y1>0) || (x1<0 && y1<0))
                acos=1-acos;
            float angle =(float)Math.toDegrees(acos);
            if (x1<0 && y1>0)
                angle+=180;
            if (x1>0 && y1>0)
                angle+=120;
            if (x1<0 && y1<0)
                angle+=90;
            if (x1<0 && y1<0)
                angle+=220;
            //Создаем стрелу с нужным углом поворота
            Arrow tmp=new Arrow(startPos,angle);
            arrows.add(tmp);
            //Меняем позицию стрелы, пока она не достигла противника
            while (Math.sqrt(Math.pow(tmp.getPosition().getX()-startPos.getX(),2)+Math.pow(tmp.getPosition().getY()-startPos.getY(),2)) < distance) {
                tmp.setCurPos(new Position(tmp.getPosition().getX()+directionX * step * 0.15f, tmp.getPosition().getY()+directionY * step * 0.15f));
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //Удаляем стрелу
            arrows.remove(tmp);
            return;
        }
    }

    @Override
    public void draw(SpriteBatch batch) {
        PlayerView pw = (PlayerView) getView();
        Player pm = (Player) getModel();
        pw.draw(batch, pm.getPosition(), pm.getCurrentHp(), pm.getHp(), pm.getLevel(), pm.getExperience(), pm.getAgility(), pm.getPower(), pm.getDefense());
        if (inventoryOpened) {
            pw.drawInventory(batch, getModel().getInventory());
        }
    }

    @Override
    public Player getModel() {
        Player p=(Player)super.getModel();
        return p;
    }
    @Override
    public PlayerView getView() {
        PlayerView p=(PlayerView)super.getView();
        return p;
    }
}
