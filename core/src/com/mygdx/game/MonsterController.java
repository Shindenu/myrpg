package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Random;

/**
 * Монстр
 */
public class MonsterController extends UnitController{

    //Эффект накладываемый после смерти на противника
    private EffectController effect;

    MonsterController (MobType type){
        String texture = new String();
        super.setModel(new Monster());
        if (type==MobType.SCYTHE)
            texture="scythe_mob.png";
        if (type==MobType.AXE)
            texture="axe_mob.png";
        super.setView(new MonsterView(texture));
        this.effect=new EffectController();
    }

    /**
     * Реакция на атаку
     * @param aggressor - противник
     */
    public void react (UnitController aggressor){
        if (this.getModel().getCurrentHp()<1){
            this.getExperience((PlayerController) aggressor);
            this.getEffect((PlayerController) aggressor);
            this.die();
        }
        else
            attack(aggressor);
    }

    /**
     * Дать игроку опыт за убийство
     * @param player - игрок
     */
    public void getExperience (PlayerController player){
        player.getModel().setExperience(player.getModel().getExperience()+this.getModel().getPrice());
    }


    /**
     * Дать игроку эффект
     * @param player - игрок
     */
    public void getEffect (PlayerController player){
        effect.putOnAPlayer(player);
    }

    @Override
    public void draw(SpriteBatch batch) {
        MonsterView pw = getView();
        Monster pm = getModel();
        pw.draw(batch, pm.getPosition(), pm.getCurrentHp() , pm.getHp());
    }

    @Override
    public Monster getModel(){
        Monster m;
        m=(Monster)super.getModel();
        return m;
    }
    @Override
    public MonsterView getView(){
        MonsterView m;
        m=(MonsterView)super.getView();
        return m;
    }
}
