package com.mygdx.game;

import java.util.Random;

/**
 * Позиция
 */
public class Position {
    private float x = 0;
    private float y = 0;

    Position(){}
    Position (float X, float Y)
    {
        x=X;
        y=Y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    /**
     * Получить рандомную позицию
     * @return - новая случайная позиция
     */
    public static Position getRandomPos(){
        Position pos=new Position();
        Random rand = new Random();
        pos.setX(rand.nextInt(1250-100));
        pos.setY(rand.nextInt(700-100));
        return pos;
    }

    /**
     * Определить находится ли данная позиция в доступном радиусе
     * @param pos - позиция
     * @param range - радиус
     * @return находится ли данная позиция в доступном радиусе
     */
    public boolean isInRange(Position pos, int range){
          if (((this.getX()-range)<pos.getX() && this.getX()+range>pos.getX()) &&
                  ((this.getY()-range)<pos.getY() && this.getY()+range>pos.getY()))
              return true;
          else
              return false;
    }
}
