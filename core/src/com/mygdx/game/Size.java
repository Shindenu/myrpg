package com.mygdx.game;

/**
 * Размер
 */
public class Size {
    public int x;
    public int y;

    Size (int x, int y)
    {
        setX(x);
        setY(y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}

