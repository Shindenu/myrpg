package com.mygdx.game;

/**
 * Предмет
 */
public class Item {
    private String name;
    private int hp;
    private int power;
    private int agility;
    private Position position;
    private Size size;

    Item (String Name, int HP, int Power, int Agility){
        name=Name;
        hp=HP;
        power=Power;
        agility=Agility;
        setSize(new Size(30,60));
        setPosition(Position.getRandomPos());
    }

    public Item() {
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getAgility() {
        return agility;
    }

    public int getHp() {
        return hp;
    }

    public int getPower() {
        return power;
    }

    public Position getPosition() {
        return position;
    }

    public Size getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}
