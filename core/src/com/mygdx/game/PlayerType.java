package com.mygdx.game;

/**
 * Класс игрока
 */
public enum PlayerType {
    KNIGHT, ARCHER;
}
