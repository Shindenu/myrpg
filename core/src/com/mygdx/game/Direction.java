package com.mygdx.game;

/**
 * Направление движения
 */
public enum Direction {
    NORTH, SOUTH, WEST, EAST;

    /**
     * Преобразовать число в направление
     * @param num - число
     * @return - направление
     */
    public static Direction fromInt (int num){
        switch (num) {
            case 1:
                return NORTH;
            case 2:
                return SOUTH;
            case 3:
                return WEST;
            case 4:
                return EAST;
            default:
                return null;
        }
    }

}

